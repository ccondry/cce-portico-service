const hydraExpress = require('hydra-express')
const hydra = hydraExpress.getHydra()
var config = require('./config.json')

var serviceName = config.hydra.serviceName
config.hydra.serviceName = 'ccc-portico-client'

hydraExpress.init(config, () => {})
.then((serviceInfo) => {
  console.log('serviceInfo', serviceInfo)

  hydra.on('message', (message) => {
    console.log('message reply', message)
  })

  let message = hydra.createUMFMessage({
    to: serviceName + ':/',
    frm: hydra.getServiceName() + ':/',
    bdy: {
      host: 'ccecall.dcloud.cisco.com',
      rejectUnauthorized: false
    }
  })

  hydra.sendMessage(message);
})
.catch(err => console.log('err', err))
