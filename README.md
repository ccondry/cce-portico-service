# cce-portico-service
A hydra microservice that can interact with the Cisco CCE Diagnostic Framework Portico HTTP API.

## Usage
Start the service with `node src/server`

Start the example client with `node src/client` after you have set up `client.config.json` file. Copy or rename `client.config.sample.json` to `client.config.json` and put in your API info.

Or, you can use hydra-cli to send

## Test
Run mocha tests with `npm test`

Test requires `client.config.json` to provide API auth info. Copy or rename `client.config.sample.json` to `client.config.json` and put in your API info to run successful tests.
