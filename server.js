const hydraExpress = require('hydra-express')
const hydra = hydraExpress.getHydra()
const config = require('./config.json')
const portico = require('cce-diagnostic-portico')
const env = require('node-env-file')

// load environment file
env(__dirname + '/.env')

function onRegisterRoutes() {
  var express = hydraExpress.getExpress()
  var api = express.Router()

  api.get('/processes', function(req, res) {
    // console.log('hydraExpress request body:', req.body)
    // console.log('hydraExpress request params:', req.params)
    console.log('hydraExpress request query:', req.query)
    // process the URL parameter back into a boolean
    let rejectUnauthorized
    if (req.query.rejectUnauthorized) {
      rejectUnauthorized = req.query.rejectUnauthorized === 'true'
    }
    portico.listProcesses({
      host: req.query.host,
      username: req.query.username || process.env.portico_user,
      password: req.query.password || process.env.portico_pass,
      rejectUnauthorized: rejectUnauthorized || false
    })
    .then(response => {
      console.log(response)
      res.status(200).send({response})
    })
    .catch(error => {
      console.log(error)
      res.status(500).send({error: 'test'})
    })
  })

  hydraExpress.registerRoutes({
    '': api
  })
}

hydraExpress.init(config, onRegisterRoutes)
.then(serviceInfo => {
  console.log('serviceInfo', serviceInfo);
  return 0
})
.catch(err => console.log('err', err))
