const hydraExpress = require('hydra-express')
const hydra = hydraExpress.getHydra()
var config = require('./config.json')

var serviceName = config.hydra.serviceName
config.hydra.serviceName = 'ccc-portico-client'

hydraExpress.init(config, () => {})
.then(serviceInfo => {
  let host = 'ccecall.dcloud.cisco.com'
  let rejectUnauthorized = false
  let message = hydra.createUMFMessage({
    to: `${serviceName}:[get]/processes?host=${host}&rejectUnauthorized=${rejectUnauthorized}`,
    from: hydra.getServiceName() + ':/',
    body: {}
  })

  return hydra.makeAPIRequest(message)
  .then(response => {
    console.log('response', response)
  })
})
.catch(err => console.log('err', err))
